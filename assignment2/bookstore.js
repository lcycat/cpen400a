var cart = {

};

var productList = ["Box1","Box2","Clothes1","Clothes2","Jeans","Keyboard","KeyboardCombo","Mice","PC1","PC2","PC3","Tent"];

var inactiveTime = 0;

var products = {

}


function showCart(){
	var counter = 0;
	var cartLoad = Object.keys(cart);
	if(cartLoad.length==0){
		alert("You havn't put anything in your cart yet!");
	}
	else{
	alert(cartLoad[counter] + ' X ' + cart[cartLoad[counter]]);
	counter++;
         setInterval(function(){
			 if(counter < cartLoad.length){
			alert(cartLoad[counter] + ' X ' + cart[cartLoad[counter]]);
			counter++;
		}	 
		 }, 30000);
	}
	inactiveTime=0;
}


//check activeTime
function checkTimeout(){
	if(inactiveTime>30){
		alert("Hey there! Are you still planning to buy something?");
		inactiveTime=0;
	}
	else{
		inactiveTime++;
	}
	console.log("The inactive time is "+inactiveTime);
	
}

//add a product in cart
function addToCart(){
console.log("Add clicked");
var proDiv =this.parentNode;
var pName = proDiv.lastChild.textContent;

if(cart[pName]+1>products[pName]){
	alert(pName+" is out of store, you cannot put more in your cart!");
}
else if(cart[pName]==null){
	cart[pName]=1;
}
else{
	cart[pName]++;
}
inactiveTime=0;
console.log("The # of "+pName+" is "+cart[pName]);
};

//remove a product in cart
function removeFromCart(){
console.log("Remove clicked");
var proDiv =this.parentNode;
var pName = proDiv.lastChild.textContent;

if(cart[pName]==null){
	alert("There is no "+pName+" in your cart.");
}	
else if(cart[pName]==1){
	delete cart[pName];
}
else{
	cart[pName]--;
}
inactiveTime=0;
};

//set a number of all the Products
function setAll(q){
	for(var i =0 ; i<productList.length ; i++){
		products[productList[i]]= q;	
	}
	
}

//Start
var init = function(){
//set the number of all the products to 10
setAll(10);

//get elements
var cartLogo = document.getElementById('cart');
var addButtons = document.getElementsByClassName("addButton");
var removeButtons = document.getElementsByClassName('removeButton');
console.log(addButtons.length);

//assign functions
cartLogo.addEventListener("click", showCart);

 for(i=0; i<addButtons.length; i++) {
        addButtons[i].addEventListener('click',addToCart);
    }
	
 for(i=0; i<removeButtons.length; i++) {
        removeButtons[i].addEventListener('click',removeFromCart);
    }

setInterval(checkTimeout,1000);

console.log("initialize down");
	
}
