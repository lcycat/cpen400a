var express = require('express')
var app = express()
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/cpen400";

var productsFromDB=[];
var ordersFromDB;
var productsFromServer;
// Connect to the db


	

var findProducts = function(db, callback) {
   var cursor =db.collection('products').find( );
   cursor.each(function(err, doc) {
      if (doc != null) {
        productsFromDB = productsFromDB.concat(doc);
      } else {
         callback();
      }
   });
};
   


var findOrders = function(db, callback) {
   var cursor =db.collection('orders').find( );
   cursor.each(function(err, doc) {
      if (doc != null) {
        ordersFromDB = ordersFromDB.concat(doc);
      } else {
         callback();
      }
   });
};



app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


app.get('/products', function(request, response) {

  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  
  productsFromDB=[];
  
  MongoClient.connect(url, function(err, db) {
    if(err) { return console.dir(err); }
     
	  findProducts(db, function() {
      db.close();
	  response.json(productsFromDB);
	  console.log("product list returned");
	  
    });
    });
	
  
    

});



app.post('/checkout',function(request, response) {


  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  console.log(request.body);
  var order = Object.keys(request.body)[0];
  
  if(order!=null){
	  var order = JSON.parse(order);
	  console.log(typeof(order));
	  
	  
	  MongoClient.connect(url, function(err, db) {
		 if(err) { return console.dir(err); }
		 
		 //db.collection('orders').remove();
		 db.collection('orders').insert(order);
		 
		 //cardLoad is the JSON cart info we get from client
		/**
		   the structure of order is:
		   {
			'cart':{mice:1,keyboard:2,...},
			'total': 456		
		   }
		  
		  **/
		 var cartLoad = order['cart'];
		  console.log(cartLoad);
		  var cartList = Object.keys(cartLoad);
		  console.log(cartList);
		  var cartQuantity = [];
		  
		  for(var i=0; i<cartList.length; i++){
			cartQuantity.push(cartLoad[cartList[i]]);
		  }
		   console.log(cartQuantity);

		  for(var j=0; j<cartList.length; j++){

			db.collection('products').update(
			  {"name" : cartList[j] },
			  {
				$inc: {"quantity" : -cartQuantity[j]}
			  });
		  }
		  
	  });
      response.status(200).send("received!");
    }
	else{
	response.status(200).send("received empty cart!");
	}
  
});



app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})

//var productsList = Object.keys(productsFromServer);

