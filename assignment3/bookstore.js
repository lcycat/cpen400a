var cart = {

};

var totalPrice = 0;

var productList = ["Box1","Box2","Clothes1","Clothes2","Jeans","Keyboard","KeyboardCombo","Mice","PC1","PC2","PC3","Tent"];

var inactiveTime = 300;

var products = {
	'Box1' : {
		'price' : 10,
		'quantity':0
	},
	'Box2' : {
		'price' : 5,
		'quantity':0
	},
	'Clothes1' : {
		'price' : 20,
		'quantity':0
	},
	'Clothes2' : {
		'price' : 30,
		'quantity':0
	},
	'Jeans' : {
		'price' : 50,
		'quantity':0
	},
	'Keyboard' : {
		'price' : 20,
		'quantity':0
	},
	'KeyboardCombo' : {
		'price' : 40,
		'quantity':0
	},
	'Mice' : {
		'price' : 20,
		'quantity':0
	},
	'PC1' : {
		'price' : 350,
		'quantity':0
	},
	'PC2' : {
		'price' : 400,
		'quantity':0
	},
	'PC3' : {
		'price' : 300,
		'quantity':0
	},
	'Tent' : {
		'price' : 100,
		'quantity':0
	}
};

var priceDisplay;
var priceDisplay2;
var inactTimer;
var modal
var modalbg;

function showCart(){
	var counter = 0;
	var cartLoad = Object.keys(cart);
	if(cartLoad.length==0){
		alert("You havn't put anything in your cart yet!");
	}
	else{
		alert(cartLoad[counter] + ' X ' + cart[cartLoad[counter]]);
		counter++;
		setInterval(function(){
			if(counter < cartLoad.length){
				alert(cartLoad[counter] + ' X ' + cart[cartLoad[counter]]);
				counter++;
			}	 
		}, 30000);
	}
	inactiveTime=300;
}


//check activeTime
function checkTimeout(){
	if(inactiveTime==0){
		alert("Hey there! Are you still planning to buy something?");
		inactiveTime=300;
	}
	else{
		inactiveTime--;
	}
	inactTimer.innerHTML = inactiveTime;
}

//add a product in cart
function addToCart(){
	console.log("Add clicked");
	var pName
	var proDiv
	console.log("the class name is "+this.className);
	if(this.className=='modalB'){
		pName = this.id;
		var spanS =document.getElementsByTagName("span");
		for (var i = 0; i < spanS.length; i++) {
         if (spanS[i].textContent == pName) {
          proDiv = spanS[i].parentNode;
          break;
                                            }
                                            }
	}
	else{
    proDiv =this.parentNode;
	pName = proDiv.lastChild.textContent;
	}

	if(cart[pName]+1>products[pName].quantity){
		alert(pName+" is out of store, you cannot put more in your cart!");
	}
	else if(cart[pName]==null){
		cart[pName]=1;
		totalPrice = totalPrice+products[pName].price;
		console.log(totalPrice);
		proDiv.children[4].style.visibility = 'visible';
		priceDisplay.innerHTML = totalPrice;
		priceDisplay2.innerHTML = totalPrice;
	}
	else{
		cart[pName]++;
		totalPrice = totalPrice+products[pName].price;
		console.log(totalPrice);
		priceDisplay.innerHTML = totalPrice;
		priceDisplay2.innerHTML = totalPrice;
	}
	inactiveTime=300;
	updateModel();
	console.log("The # of "+pName+" is "+cart[pName]);
};

//remove a product in cart
function removeFromCart(){
	console.log("Remove clicked");
	var pName
	var proDiv
	console.log("the class name is "+this.className);
	if(this.className=='modalB'){
		pName = this.id;
		var spanS =document.getElementsByTagName("span");
		for (var i = 0; i < spanS.length; i++) {
         if (spanS[i].textContent == pName) {
          proDiv = spanS[i].parentNode;
          break;
                                                }
                                            }
	}
	else{
    proDiv =this.parentNode;
	pName = proDiv.lastChild.textContent;
	}

	if(cart[pName]==null){
		alert("There is no "+pName+" in your cart.");
	}	
	else if(cart[pName]==1){
		delete cart[pName];
		totalPrice = totalPrice-products[pName].price;
		proDiv.children[4].style.visibility = 'hidden';
		priceDisplay.innerHTML = totalPrice;
		priceDisplay2.innerHTML = totalPrice;
	}
	else{
		cart[pName]--;
		totalPrice = totalPrice-products[pName].price;
		priceDisplay.innerHTML = totalPrice;
		priceDisplay2.innerHTML = totalPrice;
	}
	inactiveTime=300;
	updateModel();
};

//functions of cart modal
function showModal(){
modal.style.display = "block";
modalbg.style.height = parseFloat(document.getElementById('modal').clientHeight)+ 20+"px";
}

function closeModal(){
modal.style.display = "none";
}

function updateModel(){
	var modalList = document.getElementById("cartTable");
	//clear the list
	console.log(modalList.rows.length);
	var len = modalList.rows.length;
	while(modalList.rows.length>1){
		modalList.deleteRow(1);
	}
	console.log(modalList.rows.length);

	//load the list
	var cartLoad = Object.keys(cart);
	for(var i=0; i<cartLoad.length;i++){
		var proRow = document.createElement("tr");
		var proName = document.createElement("td");
		var proQua = document.createElement("td");
		var proButton = document.createElement("td");
		var proAdd = document.createElement("img");
		var proRemove = document.createElement("img");
		
		
		
		proAdd.src="images/add.png";
		proRemove.src="images/minus.png";
		proAdd.className="modalB";
		proRemove.className="modalB";
		proAdd.id=cartLoad[i];
		proRemove.id=cartLoad[i];
		proRow.className = "modalItem";
		proRow.id = cartLoad[i];
		proName.innerHTML = cartLoad[i];
		proQua.innerHTML = cart[cartLoad[i]];
		
		
		
		proAdd.addEventListener('click', addToCart);
        proRemove.addEventListener('click', removeFromCart);
		
		
        proButton.appendChild(proRemove);
		proButton.appendChild(proAdd);
		proRow.appendChild(proName);
		proRow.appendChild(proQua);
		proRow.appendChild(proButton);
		modalList.appendChild(proRow);
		
		modalbg.style.height = parseFloat(document.getElementById('modal').clientHeight)+ 20+"px";

	}
	
}

window.onkeyup = function(){
	if(event.keyCode == 27){
		if(modal.style.display == "block"){
			closeModal();
		}
		
	}
}

//set a number of all the Products
function setAll(q){
	for(var i =0 ; i<productList.length ; i++){
		products[productList[i]].quantity= q;	
	}
	
}

//Start
var init = function(){
//set the number of all the products to 10
setAll(10);

//get elements
var cartLogo = document.getElementById('cart');
var addButtons = document.getElementsByClassName("addButton");
var removeButtons = document.getElementsByClassName('removeButton');
priceDisplay = document.getElementById('price');
priceDisplay2 = document.getElementById('pricet');
var cartPrice = document.getElementById('cartPrice');
modal = document.getElementById('modalwindow');
modalbg = document.getElementById('modalbg');
var closeM = document.getElementById('closeM');
inactTimer = document.getElementById('inactiveTime');



//assign functions
cartLogo.addEventListener("click", showCart);
cartPrice.addEventListener('click', showModal);
closeM.addEventListener('click', closeModal);

for(i=0; i<addButtons.length; i++) {
	addButtons[i].addEventListener('click',addToCart);
}

for(i=0; i<removeButtons.length; i++) {
	removeButtons[i].addEventListener('click',removeFromCart);
}

modalbg.style.height = parseFloat(document.getElementById('modal').clientHeight)+ 20+"px";
priceDisplay.innerHTML = totalPrice;
updateModel();

setInterval(checkTimeout,1000);
inactTimer.innerHTML = inactiveTime;

console.log("initialize down");

}
